class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email, index: { unique: true }, null: false
      t.string :external_id
      t.integer :role

      t.timestamps
    end
  end
end

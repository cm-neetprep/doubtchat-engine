# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_23_052542) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "doubt_chat_channels", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "doubt_chat_doubt_answers", force: :cascade do |t|
    t.integer "doubt_chat_user_id", null: false
    t.integer "doubt_chat_doubt_id", null: false
    t.json "content", null: false
    t.string "ancestry"
    t.integer "upvote_count", default: 0
    t.integer "downvote_count", default: 0
    t.boolean "deleted", default: false
    t.string "display_parent_id"
    t.integer "display_parent_position"
    t.integer "children_count", default: 0, null: false
    t.boolean "accepted_answer", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ancestry"], name: "index_doubt_chat_doubt_answers_on_ancestry"
    t.index ["doubt_chat_doubt_id"], name: "index_dcda_on_dcd_id"
    t.index ["doubt_chat_user_id"], name: "index_dcda_on_dcu_id"
  end

  create_table "doubt_chat_doubts", force: :cascade do |t|
    t.integer "doubt_chat_user_id", null: false
    t.integer "doubt_chat_channel_id", null: false
    t.json "content", null: false
    t.integer "upvote_count", default: 0
    t.integer "downvote_count", default: 0
    t.boolean "deleted", default: false
    t.integer "doubt_answers_count", default: 0, null: false
    t.integer "accepted_doubt_answer_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["doubt_chat_channel_id"], name: "index_dcd_on_dcc_id"
    t.index ["doubt_chat_user_id"], name: "index_dcd_on_dcu_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", null: false
    t.string "external_id"
    t.integer "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.string "votable_type"
    t.integer "votable_id"
    t.string "voter_type"
    t.integer "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["votable_type", "votable_id"], name: "index_votes_on_votable_type_and_votable_id"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
    t.index ["voter_type", "voter_id"], name: "index_votes_on_voter_type_and_voter_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "doubt_chat_doubt_answers", "doubt_chat_doubts"
  add_foreign_key "doubt_chat_doubt_answers", "doubt_chat_users"
  add_foreign_key "doubt_chat_doubts", "doubt_chat_channels"
  add_foreign_key "doubt_chat_doubts", "doubt_chat_users"
end

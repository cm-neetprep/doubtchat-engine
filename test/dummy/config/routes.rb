Rails.application.routes.draw do
  mount DoubtChat::Engine => "/doubt-chat"
  knockknock_for :users, class_name: "DoubtChat::User", controllers: {
    registrations: 'api/users/registrations',
    tokens: 'api/users/tokens',
  }, as: ""
  namespace :users do
    get :me, to: 'users#me'
  end
end

DoubtChat::Engine.routes.draw do

  shallow do
    resources :attachments, only: [:destroy]
    resources :channels, only: [:index, :show] do
      resources :doubts, only: [:index, :show, :create, :update, :destroy] do
        member do
          post :upvote
          post :downvote
        end
        resources :doubt_answers, path: "doubt-answers", only: [:index, :show, :create, :update, :destroy] do
          member do
            get :replies
            post :upvote
            post :downvote
            put :accept
          end
        end
      end
    end
  end

end

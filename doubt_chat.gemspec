$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "doubt_chat/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "doubt_chat"
  spec.version     = DoubtChat::VERSION
  spec.authors     = ["Dinesh Gadge"]
  spec.email       = ["dinesh@dineshgadge.com"]
  spec.homepage    = "https://gitlab.com/cm-neetprep/doubtchat-engine"
  spec.summary     = "An engine to run doubts as a chat service."
  spec.description = "An engine to run doubts as a chat service."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    # TODO: Change this
    spec.metadata["allowed_push_host"] = "http://mygemserver.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  # spec.add_dependency "rails", "~> 5.2.3"
  spec.add_dependency "acts_as_votable"
  spec.add_dependency 'counter_culture', '~> 2.0'
  spec.add_dependency 'fast_jsonapi'
  spec.add_dependency 'oj'
  spec.add_dependency 'pagy'
  spec.add_dependency 'pusher'
  spec.add_dependency 'ancestry'

  spec.add_development_dependency "sqlite3"
end

# DoubtChat
Short description and motivation.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'doubt_chat', git: 'https://gitlab.com/cm-neetprep/doubtchat-engine', branch: 'master'
```

And then execute:
```bash
$ bundle
```

In your `config/routes.rb` file add:
```ruby
namespace :api do
  mount DoubtChat::Engine => "/doubt-chat"
end
```

In your `config/initializers/doubt_chat.rb` file add:
```ruby
require 'doubt_chat'
DoubtChat.setup do |config|
  config.user_class_name = 'User'
  config.base_controller_name = '::ApplicationController'
end
```

The engine assumes that `current_user` and `authenticate_user!` helpers are available
( Provided by knockknock )

On the terminal run
```bash
rails doubt_chat:install:migrations
rails db:migrate
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

class CreateDoubtChatDoubtAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :doubt_chat_doubt_answers do |t|
      t.bigint :doubt_chat_user_id, null: false, index: {name: "index_dcda_on_dcu_id"}
      t.references :doubt_chat_doubt, null: false, foreign_key: true, index: {name: "index_dcda_on_dcd_id"}
      t.json :content, null: false
      t.string :ancestry
      t.integer :upvote_count, default: 0
      t.integer :downvote_count, default: 0
      t.boolean :deleted, default: false
      t.string :display_parent_id
      t.integer :display_parent_position
      t.integer :children_count, default: 0, null: false
      t.boolean :accepted_answer, default: false, null: false

      t.timestamps
    end
    add_index :doubt_chat_doubt_answers, :ancestry
  end
end

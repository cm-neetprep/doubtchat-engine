class CreateDoubtChatChannels < ActiveRecord::Migration[5.2]
  def change
    create_table :doubt_chat_channels do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end

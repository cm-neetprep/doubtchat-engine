class CreateDoubtChatDoubts < ActiveRecord::Migration[5.2]
  def change
    create_table :doubt_chat_doubts do |t|
      t.bigint :doubt_chat_user_id, null: false, index: {name: "index_dcd_on_dcu_id"}
      t.references :doubt_chat_channel, null: false, foreign_key: true, index: {name: "index_dcd_on_dcc_id"}
      t.json :content, null: false
      t.integer :upvote_count, default: 0
      t.integer :downvote_count, default: 0
      t.boolean :deleted, default: false
      t.integer :doubt_answers_count, null: false, default: 0
      t.integer :accepted_doubt_answer_id
      t.timestamps
    end
  end
end

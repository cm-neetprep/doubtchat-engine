module DoubtChat
  class Engine < ::Rails::Engine
    isolate_namespace DoubtChat

    initializer "pusher" do |app|
      Pusher.app_id = ENV['PUSHER_APP_ID']
      Pusher.key = ENV['PUSHER_KEY']
      Pusher.secret = ENV['PUSHER_SECRET']
      Pusher.cluster = ENV['PUSHER_CLUSTER']
      Pusher.logger = Rails.logger
      Pusher.encrypted = true
    end

    initializer "pagy" do |app|
      require 'pagy'
      require 'pagy/extras/overflow'
      Pagy::VARS[:overflow] = :empty_page # default  (other options: :last_page and :exception)
    end

    config.to_prepare do
      if DoubtChat.config
        DoubtChat.config.user_class.class_eval do
          has_many :doubt_chat_doubts, class_name: 'DoubtChat::Doubt', foreign_key: :doubt_chat_user_id
          has_many :doubt_chat_doubt_answers, class_name: "DoubtChat::DoubtAnswer",
                           foreign_key: :doubt_chat_user_id

          acts_as_voter
        end
      end
    end
  end
end

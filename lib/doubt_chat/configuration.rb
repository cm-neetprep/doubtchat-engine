module DoubtChat
  class Configuration
    attr_accessor :user_class_name, :base_controller_name, :user_serializer_name

    def user_class
      user_class_name.classify.constantize
    end

    def base_controller
      base_controller_name.classify.constantize
    end

    def user_serializer
      user_serializer_name.classify.constantize
    end
  end
end

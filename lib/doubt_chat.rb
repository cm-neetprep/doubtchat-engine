require "acts_as_votable"
require "pagy"
require "fast_jsonapi"
require "counter_culture"
require "ancestry"
require "doubt_chat/engine"
require "doubt_chat/configuration"
require "pusher"

#require "zeitwerk"
#loader = Zeitwerk::Loader.for_gem
#loader.setup

module DoubtChat

  # mattr_accessor :user_class_name

  # def setup
  #   yield self
  # end

  class << self
    attr_reader :config

    def setup
      @config = Configuration.new
      yield config
    end
  end

end

DC = DoubtChat

#loader.eager_load

module DoubtChat
  module ChannelValidatable
    extend ActiveSupport::Concern

    private

    def set_channel
      @channel = DoubtChat::Channel.where(id: params[:id]).first
    end

    def shallow_set_channel
      @channel = DoubtChat::Channel.where(id: params[:channel_id]).first
    end

    def ensure_channel_present!
      return true if @channel.present?

      render json: { errors: ["Channel not present or user not authorized"] }, status: :forbidden
      return false
    end
  end
end

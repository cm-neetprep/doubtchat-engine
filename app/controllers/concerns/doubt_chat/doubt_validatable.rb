module DoubtChat
  module DoubtValidatable
    extend ActiveSupport::Concern

    private

    def set_doubt
      @doubt = DoubtChat::Doubt.where(id: params[:id]).includes(:last_answer).non_deleted.with_attached_attachments.first
    end

    def shallow_set_doubt
      @doubt = DoubtChat::Doubt.where(id: params[:doubt_id]).includes(:last_answer).non_deleted.first
    end

    def ensure_doubt_present!
      return true if @doubt.present?

      render json: { errors: ["Doubt not present or user not authorized"] }, status: :forbidden
      return false
    end
  end
end

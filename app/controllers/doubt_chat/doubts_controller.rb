# rubocop:disable Metrics/ClassLength
class DoubtChat::DoubtsController < DoubtChat::ApplicationController

  include DoubtChat::ChannelValidatable
  include DoubtChat::DoubtValidatable

  before_action :shallow_set_channel, only: [:index, :create]
  before_action :ensure_channel_present!, only: [:index, :create]
  before_action :set_doubt, only: [:show, :update, :destroy, :upvote, :downvote]
  before_action :ensure_doubt_present!, only: [:show, :update, :destroy, :upvote, :downvote]
  before_action :ensure_user_authorized!, only: [:update, :destroy]

  def index
    doubts = @channel.doubts.non_deleted
                     .includes(:user, [{ last_answer: :attachments_attachments }])
                     .with_attached_attachments
    if params[:sort_by] == "most-upvoted"
      doubts = doubts.order(cached_votes_score: :desc, created_at: :desc)
    else
      doubts = doubts.order(created_at: :desc)
    end

    pagination, doubts = pagy(doubts,
                              items: params[:page_size] || 25,
                              page: params[:page] || 1)

    current_user_votes = current_user.votes.where(votable: doubts)

    render json: {
      multi_data: true,
      doubts: DoubtChat::DoubtSerializer.new(
        doubts,
        { include: [:user, :last_answer, :attachments],
          params: { current_user_votes: current_user_votes },
          meta: {
            current_page: pagination.page,
            total: pagination.count,
            page_size: pagination.items,
          }, }
      ),
    }, status: :ok
  end

  def show
    render_doubt
  end

  def create
    doubt = @channel.doubts.new(doubt_chat_doubt_params.merge(doubt_chat_user_id: current_user.id))

    if doubt.save
      @doubt = doubt
      trigger_pusher
      render_doubt
      return
    end

    render_error(doubt.errors.messages)
  end

  def update
    @doubt.assign_attributes(doubt_chat_doubt_params.except(:attachments))
    attachments = doubt_chat_doubt_params[:attachments]

    if @doubt.save
      @doubt.attachments.attach(attachments) if attachments.present?
      trigger_pusher
      render_doubt
      return
    end

    render_error(@doubt.errors.messages)
  end

  def destroy
    if @doubt.update!(deleted: true)
      trigger_pusher
      render json: {
        messages: ["doubt successfully deleted"],
      }, status: :ok
      return
    end

    render_error("OOPS something went wrong")
  end

  def upvote
    # 1 Doubt already have current user like (unvote) -> doubt.unvote_by user
    # 2 Doubt dont have current user like or dislike (create like) -> doubt.upvote_by user
    # 3 Doubt have dislike of user (dislike to like) -> doubt.upvote_by user
    if current_user.voted_up_for? @doubt
      @doubt.unvote_by current_user
      trigger_pusher
      render_doubt
      return
    end

    @doubt.upvote_by current_user
    trigger_pusher
    render_doubt
  end

  def downvote
    if current_user.voted_down_for? @doubt
      @doubt.unvote_by current_user
      trigger_pusher
      render_doubt
      return
    end

    @doubt.downvote_by current_user
    trigger_pusher
    render_doubt
  end

  private

  def doubt_chat_doubt_params
    params.require(:doubt)
          .permit(:content, :deleted, attachments: [])
          .merge(content: JSON.parse(params[:doubt][:content]))
  end

  def ensure_user_authorized!
    if @doubt.doubt_chat_user_id != current_user.id
      render_error("User not authorized or doubt not present")
      return false
    end

    return true
  end

  def render_doubt
    render json: {
      multi_data: true,
      doubt: serialized_doubt,
    }, status: :ok
  end

  def render_error(error_message)
    render json: { errors: Array.wrap(error_message) }, status: :forbidden
  end

  def trigger_pusher
    # Pusher.trigger(controller_name, action_name, { message: serialized_doubt.serialized_json })

    Pusher.trigger("channels-#{channel_id}", "doubts-#{action_name}", { message: serialized_doubt.serialized_json })
  end

  def current_user_votes
    @current_user_votes ||= current_user.votes.where(votable: @doubt)
  end

  def serialized_doubt
    # Note: Serializer is one time use only.
    # In the subsequent request included values are somehow ignored

    return DoubtChat::DoubtSerializer.new(
      @doubt,
      { include: [:user, :last_answer, :attachments],
        params: { current_user_votes: current_user_votes }, }
    )
  end

  def channel_id
    return @channel.id if @channel.present?

    @channel_id ||= @doubt.doubt_chat_channel_id
  end

end
# rubocop:enable Metrics/ClassLength

# rubocop:disable Metrics/ClassLength
class DoubtChat::DoubtAnswersController < DoubtChat::ApplicationController

  include DoubtChat::DoubtValidatable

  before_action :shallow_set_doubt, only: [:index, :create]
  before_action :ensure_doubt_present!, only: [:index, :create]
  before_action :set_doubt_answer, only: [:show, :update, :destroy, :upvote, :downvote, :accept, :replies]
  before_action :ensure_doubt_answer_present!, only: [:show, :update, :destroy, :upvote, :downvote, :accept, :replies]
  before_action :ensure_user_authorized!, only: [:update, :destroy]
  before_action :accept_valid?, only: [:accept]

  def index
    doubt_answers = @doubt.doubt_answers.roots
                          .non_deleted.includes(:user)
                          .with_attached_attachments
                          .order(created_at: :desc).all

    pagination, doubt_answers = pagy(doubt_answers,
                                     items: params[:page_size] || 25,
                                     page: params[:page] || 1)

    current_user_votes_for_doubt_answers = current_user.votes.where(votable: doubt_answers)

    render json: {
      multi_data: true,
      doubt: serialized_doubt,
      doubt_answers: DoubtChat::DoubtAnswerSerializer.new(
        doubt_answers,
        {
          include: [:user, :attachments],
          params: {
            current_user_votes: current_user_votes_for_doubt_answers,
          },
          meta: {
            current_page: pagination.page,
            total: pagination.count,
            page_size: pagination.items,
          },
        }
      ),
    }, status: :ok
  end

  def show
    if @doubt_answer.doubt.deleted
      render json: { error: ["Doubt answer does not exist"] }
      return
    end

    render_doubt_answer
  end

  def create
    doubt_answer = @doubt.doubt_answers.new(doubt_answer_params.merge(doubt_chat_user_id: current_user.id))
    answer_parent_id = params[:doubt_answer][:doubt_answer_parent_id]
    doubt_answer.parent = DoubtChat::DoubtAnswer.non_deleted.where(id: answer_parent_id).first

    if doubt_answer.save
      @doubt_answer = doubt_answer
      trigger_pusher_with_channel
      render_doubt_answer
      return
    end

    render_error(doubt_answer.errors.messages)
  end

  def update
    @doubt_answer.assign_attributes(doubt_answer_params.except(:attachments))
    attachments = doubt_answer_params[:attachments]
    if @doubt_answer.save
      @doubt_answer.attachments.attach(attachments) if attachments.present?
      trigger_pusher
      render_doubt_answer
      return
    end

    render_error(@doubt_answer.errors.messages)
  end

  def destroy
    if @doubt_answer.update!(deleted: true)
      trigger_pusher_with_channel
      render json: {
        messages: ["doubt answer successfully deleted"],
      }, status: :ok
      return
    end

    render_error("OOPS something went wrong")
  end

  def replies
    doubt_answer_replies = @doubt_answer.children

    pagination, doubt_answer_replies = pagy(doubt_answer_replies,
                                            items: params[:page_size] || 25,
                                            page: params[:page] || 1)

    current_user_votes_for_replies = current_user.votes.where(votable: doubt_answer_replies)

    render json: {
      multi_data: true,
      doubt_answer: serialized_doubt_answer,
      doubt_answers_replies: DoubtChat::DoubtAnswerSerializer.new(
        doubt_answer_replies,
        {
          include: [:user, :attachments],
          params: {
            current_user_votes: current_user_votes_for_replies,
          },
          meta: {
            current_page: pagination.page,
            total: pagination.count,
            page_size: pagination.items,
          },
        }
      ),
    }, status: :ok
  end

  def upvote
    if current_user.voted_up_for? @doubt_answer
      @doubt_answer.unvote_by current_user
      trigger_pusher
      render_doubt_answer
      return
    end

    @doubt_answer.upvote_by current_user
    trigger_pusher
    render_doubt_answer
  end

  def downvote
    if current_user.voted_down_for? @doubt_answer
      @doubt_answer.unvote_by current_user
      trigger_pusher
      render_doubt_answer
      return
    end

    @doubt_answer.downvote_by current_user
    trigger_pusher
    render_doubt_answer
  end

  def accept
    @doubt_answer.update!(accepted_answer: true)
    @doubt.update!(accepted_doubt_answer_id: @doubt_answer.id)
    trigger_pusher_with_channel
    render_doubt_answer_with_doubt
  end

  private

  def set_doubt_answer
    @doubt_answer = DoubtChat::DoubtAnswer.where(id: params[:id]).non_deleted.first
  end

  def ensure_doubt_answer_present!
    return true if @doubt_answer.present?

    render json: { errors: ["Doubt answer is not present or user not authorized"] }, status: :forbidden
    return false
  end

  def doubt_answer_params
    params.require(:doubt_answer)
          .permit(:content, :deleted, attachments: [])
          .merge(content: JSON.parse(params[:doubt_answer][:content]))
  end

  def accept_valid?
    @doubt = @doubt_answer.doubt

    if @doubt.doubt_chat_user_id != current_user.id
      render_error("User not authorized or doubt answer not present")
      return false
    end

    if @doubt.accepted_doubt_answer_id.present?
      render_error("Already accepted an answer")
      return false
    end

    return true
  end

  def ensure_user_authorized!
    if @doubt_answer.doubt_chat_user_id != current_user.id
      render_error("User not authorized or doubt answer not present")
      return false
    end

    if @doubt_answer.accepted_answer.present?
      render_error("You can't edit an accepted answer")
      return false
    end

    return true
  end

  def trigger_pusher
    # Pusher.trigger(controller_name, action_name, { message: serialized_doubt_answer.serialized_json })

    Pusher.trigger("doubts-#{doubt_id}",
                   "doubt_answers-#{action_name}",
                   { message: serialized_doubt_answer.serialized_json })
  end

  def trigger_pusher_with_channel
    # Pusher.trigger(controller_name, action_name, { message: serialized_doubt_answer.serialized_json })

    trigger_pusher
    Pusher.trigger("channels-#{channel_id}",
                   "doubt_answers-#{action_name}",
                   { message: serialized_doubt_answer.serialized_json })
  end

  def channel_id
    return @doubt.doubt_chat_channel_id if @doubt.present?

    @channel_id ||= @doubt_answer.doubt.doubt_chat_channel_id
  end

  def doubt_id
    return @doubt.id if @doubt.present?

    @doubt_id ||= @doubt_answer.doubt_chat_doubt_id
  end

  def render_doubt_answer
    render json: {
      multi_data: true,
      doubt_answer: serialized_doubt_answer,
    }, status: :ok
  end

  def render_error(error_messages)
    render json: { errors: Array.wrap(error_messages) }, status: :forbidden
  end

  def render_doubt_answer_with_doubt
    render json: {
      multi_data: true,
      doubt: serialized_doubt,
      doubt_answer: serialized_doubt_answer,
    }, status: :ok
  end

  def current_user_votes
    @current_user_votes ||= current_user.votes.where(votable: @doubt_answer)
  end

  def current_user_votes_for_doubt
    @current_user_votes_for_doubt ||= current_user.votes.where(votable: @doubt)
  end

  def serialized_doubt_answer
    return DoubtChat::DoubtAnswerSerializer
           .new(@doubt_answer,
                { include: [:user, :attachments],
                  params: { current_user_votes: current_user_votes }, })
  end

  def serialized_doubt
    return DoubtChat::DoubtSerializer
           .new(@doubt,
                { include: [:user, :last_answer, :attachments],
                  params: { current_user_votes: current_user_votes_for_doubt }, })
  end

end

# rubocop:enable Metrics/ClassLength

class DoubtChat::AttachmentsController < DoubtChat::ApplicationController

  before_action :set_attachment, only: [:destroy]
  before_action :ensure_attachment_present!, only: [:destroy]
  before_action :ensure_attachment_authorized!, only: [:destroy]

  def destroy
    @attachment.purge
    @record = @attachment.record
    render_attachment
  end

  private

  def set_attachment
    @attachment = ActiveStorage::Attachment.where(id: params[:id]).first
  end

  def ensure_attachment_present!
    return true if @attachment.present?

    render json: { errors: ["Attachment not present or user not authorized"] }, status: :forbidden
    return false
  end

  def ensure_attachment_authorized!
    record = @attachment.record
    if record.try(:doubt_chat_user_id) != current_user.id
      render json: { errors: ["Attachment not present or user not authorized"] }, status: :forbidden
      return false
    end

    return true
  end

  def render_attachment
    record_name = @record.class.name.tableize.split("/").last.to_s.singularize

    render json: {
      multi_data: true,
      message: "success",
      "#{record_name}": serialized_data,
    }, status: :ok
  end

  def serialized_data
    serializer = @record.class.name + "Serializer"
    record_serializer = serializer.classify.constantize
    options = {}
    options[:params] = { current_user_votes: current_user_votes }
    # TODO: Ask sir about how to do this without using hardcode values
    options[:include] =  @record.class.name == "DoubtChat::Doubt" ? [:user, :last_answer] : [:user]

    return record_serializer.new(@record, options)
  end

  def current_user_votes
    return current_user.votes.where(votable: @record)
  end

end

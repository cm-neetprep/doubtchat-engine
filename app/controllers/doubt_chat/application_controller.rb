module DoubtChat
  class ApplicationController < DoubtChat.config.base_controller
    protect_from_forgery with: :null_session
    rescue_from ActionController::ParameterMissing, with: :handle_missing_parameters
    rescue_from JWT::ExpiredSignature, with: :deny_access
    wrap_parameters false

    before_action :authenticate_user!

    include Pagy::Backend

    def handle_missing_parameters
      head :bad_request
    end

    def deny_access
      render json: { errors: ["Token has expired"] }, status: :forbidden
      return false
    end


  end
end

class DoubtChat::ChannelsController < DoubtChat::ApplicationController

  include DoubtChat::ChannelValidatable

  before_action :set_channel, only: [:show]
  before_action :ensure_channel_present!, only: [:show]

  def index
    channels = DoubtChat::Channel.order(:created_at).all

    pagination, channels = pagy(
      channels,
      items: params[:page_size] || 100,
      page: params[:page] || 1
    )
    render json: {
      multi_data: true,
      channels: DoubtChat::ChannelSerializer.new(
        channels,
        {
          meta: {
            current_page: pagination.page,
            total: pagination.count,
            page_size: pagination.items,
          },
        }
      ),
    }, status: :ok
  end

  def show
    # Pusher.trigger("#{@channel.name.to_s.parameterize}", 'show_action', {
    #   message: DoubtChat::ChannelSerializer.new(@channel).serialized_json
    # })

    render json: {
      multi_data: true,
      channel: DoubtChat::ChannelSerializer.new(@channel),
    }, status: :ok
  end

end

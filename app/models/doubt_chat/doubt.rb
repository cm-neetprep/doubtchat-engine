class DoubtChat::Doubt < ApplicationRecord

  belongs_to :user, class_name: DoubtChat.config.user_class_name, foreign_key: 'doubt_chat_user_id'
  belongs_to :channel, class_name: "DoubtChat::Channel", foreign_key: 'doubt_chat_channel_id'

  has_many :doubt_answers, class_name: "DoubtChat::DoubtAnswer",
                           foreign_key: 'doubt_chat_doubt_id'
  has_many_attached :attachments
  has_one :last_answer, -> {order(created_at: :desc)}, class_name: "DoubtChat::DoubtAnswer",
                                                       foreign_key: 'doubt_chat_doubt_id'

  acts_as_votable

  scope :non_deleted, -> { where(deleted: false) }
  scope :deleted, -> { where(deleted: true) }

  def attachment_ids
    self.attachments.map(&:id)
  end

end

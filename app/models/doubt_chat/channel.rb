class DoubtChat::Channel < ApplicationRecord

  has_many :doubts, class_name: "DoubtChat::Doubt", foreign_key: 'doubt_chat_channel_id'
  validates :name, uniqueness: { case_sensitive: false }

end

class DoubtChat::DoubtAnswer < ApplicationRecord

  belongs_to :user, class_name: DoubtChat.config.user_class_name, foreign_key: "doubt_chat_user_id"
  belongs_to :doubt, class_name: "DoubtChat::Doubt", foreign_key: "doubt_chat_doubt_id"
  counter_culture :doubt, column_name: proc {|doubt_answer|
    'doubt_answers_count' if doubt_answer.ancestry.blank? and not doubt_answer.deleted
  }

  has_many_attached :attachments
  has_ancestry counter_cache: true

  # has_many :votes, as: :voted_on
  acts_as_votable

  scope :non_deleted, -> { where(deleted: false) }
  scope :deleted, -> { where(deleted: true) }

  def attachment_ids
    self.attachments.map(&:id)
  end

end

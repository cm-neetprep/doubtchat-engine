class DoubtChat::OldUser < ApplicationRecord

  has_many :doubts, class_name: "DoubtChat::Doubt",
                    foreign_key: 'doubt_chat_user_id'

  has_many :doubt_answers, class_name: "DoubtChat::DoubtAnswer",
                           foreign_key: 'doubt_chat_user_id'

  # has_many :votes
  acts_as_voter

  has_one_attached :avatar_url

  validates :email, uniqueness: { message: "already exists" }

  def jwt
    return Knockknock::AuthToken.new(payload: self.to_token_payload).token if self.respond_to? :to_token_payload

    return Knockknock::AuthToken.new(payload: { sub: self.id }).token
  end

  def authenticate(_password)
    return true
  end

end

class DoubtChat::DoubtSerializer < DoubtChat::ApplicationSerializer

  belongs_to :user, id_method_name: :doubt_chat_user_id, serializer: DoubtChat.config.user_serializer
  has_one :last_answer, serializer: DoubtChat::DoubtAnswerSerializer
  has_many :attachments, serializer: DoubtChat::AttachmentSerializer

  attributes :id, :content, :doubt_answers_count, :accepted_doubt_answer_id,
            :deleted, :created_at
  attribute :votes, &:weighted_score
  attribute :channel_id, &:doubt_chat_channel_id

  # attribute :last_answer do |doubt|
  #   doubt_answer = doubt.last_answer
  #   next nil if doubt_answer.blank?

  #   { id: doubt_answer.id,
  #     created_at: doubt_answer.created_at,
  #     updated_at: doubt_answer.updated_at,
  #     user: doubt_answer.user, }
  # end

  attribute :current_user_vote do |doubt, params|
    vote = params[:current_user_votes].to_a.detect { |cuv| cuv.votable_id == doubt.id }
    next nil if vote.blank?

    vote.vote_flag
  end

end

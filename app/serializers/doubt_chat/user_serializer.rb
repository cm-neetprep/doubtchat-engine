class DoubtChat::UserSerializer < DoubtChat::ApplicationSerializer

  attributes :id, :name, :email
  attribute :jwt, if: param?(:include_jwt)

end

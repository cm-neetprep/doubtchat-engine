class DoubtChat::DoubtAnswerSerializer < DoubtChat::ApplicationSerializer

  belongs_to :user, id_method_name: :doubt_chat_user_id, serializer: DoubtChat.config.user_serializer
  has_many :attachments, serializer: DoubtChat::AttachmentSerializer
  attributes :id, :content,
             :children_count, :doubt_chat_doubt_id, :accepted_answer, :deleted
  attribute :votes, &:weighted_score

  attribute :current_user_vote do |doubt_answer, params|
    vote = params[:current_user_votes].to_a.detect {|cuv| cuv.votable_id == doubt_answer.id}
    next nil if vote.blank?

    vote.vote_flag
  end

  # attribute :attachments do |doubt_answer|
  #   doubt_answer.attachments.map do |attachment|
  #     {
  #       id: attachment.id,
  #       name: attachment.blob.filename.to_s,
  #       url: attachment.service_url,
  #       content_type: attachment.blob.content_type.to_s,
  #       byte_size: attachment.blob.byte_size,
  #     }
  #   end
  # end

end

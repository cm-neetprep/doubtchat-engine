class DoubtChat::AttachmentSerializer < DoubtChat::ApplicationSerializer

  attribute :id
  attribute :name do |attachment|
    attachment.blob.filename.to_s
  end
  attribute :url do |attachment|
    attachment.service_url
  end
  attribute :content_type do |attachment|
    attachment.blob.content_type.to_s
  end
  attribute :byte_size do |attachment|
    attachment.blob.byte_size
  end

  # attribute :attachments do |doubt|
  #   doubt.attachments.map do |attachment|
  #     {
  #       id: attachment.id,
  #       name: attachment.blob.filename.to_s,
  #       url: attachment.service_url,
  #       content_type: attachment.blob.content_type.to_s,
  #       byte_size: attachment.blob.byte_size,
  #     }
  #   end
  # end

end
